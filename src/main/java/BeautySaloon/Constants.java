package BeautySaloon;

public class Constants {
    public static final String INSERT_USER = "INSERT INTO user" + " (full_name, login, password) VALUES "
            + " (?, ?, ?);";
    public static final String SELECT_USER_BY_ID = "SELECT id, full_name, login, password from user where id =?;";
    public static final String SELECT_ALL_USERS = "SELECT * FROM user;";
    public static final String DELETE_USER = "DELETE FROM user WHERE id =?;";
    public static final String UPDATE_USER = "UPDATE user set full_name = ?, login = ?, password=? WHERE id = ?;";

}
