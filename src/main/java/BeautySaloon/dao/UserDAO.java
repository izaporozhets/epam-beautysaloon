package BeautySaloon.dao;

import BeautySaloon.Constants;
import BeautySaloon.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    private String jdbcURL = "jdbc:mysql://127.0.0.1:3306/?user=root";
    private String jdbcUsername = "root";
    private String jdbcPassword = "root";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

    public void insertUser(User user) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(Constants.INSERT_USER);
        preparedStatement.setString(1, user.getFullName());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.executeUpdate();
    }

    public boolean updateUser(User user) throws SQLException {
        boolean rowUpdated;
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(Constants.UPDATE_USER);
        preparedStatement.setString(1, user.getFullName());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPassword());

        return rowUpdated = preparedStatement.executeUpdate() > 0;
    }

    public User selectUser(int id) throws SQLException {
        User user = null;
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(Constants.SELECT_USER_BY_ID);
        preparedStatement.setInt(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            String name = rs.getString("full_name");
            String login = rs.getString("login");
            String password = rs.getString("password");
            user = new User(id, name, login, password);
        }
        return user;
    }

    public List<User> selectAllUsers() throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(Constants.SELECT_ALL_USERS);
        ResultSet rs = preparedStatement.executeQuery();
        List<User> allUsers = new ArrayList<>();
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("full_name");
            String login = rs.getString("login");
            String password = rs.getString("password");
            allUsers.add(new User(id, name, login, password));
        }
        return allUsers;
    }

    public boolean deleteUser(int id) throws SQLException {
        boolean rowDeleted;
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(Constants.DELETE_USER);
        preparedStatement.setInt(1, id);
        rowDeleted = preparedStatement.executeUpdate() > 0;
        return rowDeleted;
    }
}
